﻿using System.Text;
//using Microsoft.VisualStudio.TestTools.UnitTesting;
using NUnit.Framework;

using System.Collections.Generic;
using System.Linq;
using NUnit.Framework.Internal;




public class Character
{
    public string Name { get; private set; }
    public int Health { get; set; }

    public Guid RaceId { get; private set; }

    public bool isDead => Health <= 0;

    public Character(string name,Race race)
    { Name = name;
    RaceId = race.ID;
        Health = 100;
    }

   
}

public enum RaceNames
{
    Orc,
    Elf,
    Human,
    Dwarf,
    Gnome
}

public class Race
{
    public string Name { get; private set; }
    public Guid ID { get; private set; }

    public Race(string name)
    {
        Name = name;
        ID = Guid.NewGuid();
    }

    public Race(RaceNames raceName)
    {
        Name = raceName.ToString();
        ID = Guid.NewGuid();
    }
}

public static class LINQTests
{
    public static readonly List<Race> Races = new()
    {
        new Race(RaceNames.Orc),
        new Race(RaceNames.Elf),
        new Race(RaceNames.Human),
        new Race(RaceNames.Dwarf),
        new Race(RaceNames.Gnome)

    };

    public static Guid GetRaceId(RaceNames name) =>
        Races.Find(el => el.Name == name.ToString())?.ID ?? Guid.Empty;

    public static List<Character> Characters = new()
    {
        new Character("Artur Pendragon",Races[2]){Health=98 },
        new Character("Legolas",Races[1]){Health=100 },
        new Character("Gimli",Races[1]){Health=100 },
        new Character("Gandalf",Races[4]){Health=100 },
        new Character("Frodo",Races[3]){Health=100 },
        new Character ("Julius Caesar",Races[2]){Health=100 },
    };


    public static void printCharacters()
    {
        var sb = new StringBuilder();

        sb.AppendLine(String.Format("> {0,25} {1,40} {2,10} {3,7} {4,6}\n", "Name", "RaceId", "RaceName", "Health", "IsDead\n"));

        Characters.ForEach(character
            => sb.AppendLine(String.Format("> {0,25} {1,40} {2,10} {3,7} {4,6}\n",
            character.Name,
            character.RaceId.ToString(),
            Races.Find(race => race.ID == character.RaceId)?.Name ?? "Unknown",
            character.Health.ToString(),
            character.isDead.ToString()
            )));


        Console.WriteLine(sb.ToString());

    }

    [Test]
    public static void TestCharactersDictionary()
    {
        var charactersDict = Characters.ToDictionary(
            character => character.Name,
            character => character
            );
        Assert.That(charactersDict.Keys, Is.All.TypeOf<string>());

    }

    [Test]
    public static void TestCharacterSortedList()
    {
        var charactersSortedList = Characters.ToSortedList(character => character.Health);
        Assert.That(charactersSortedList.Values, Is.Ordered.By("Health"));
    }

    [Test]
    public static void TectCharacterQueue()
    {
        var charactersQueue = new Queue<Character>(Characters);
        Assert.That(charactersQueue.Count, Is.EqualTo(Characters.Count));
    }
    [Test]
    public static void TestCharacterStack()
    {
        var charactersStack = new Stack<Character>(Characters);
        Assert.That(charactersStack.Count, Is.EqualTo(Characters.Count));
    }

    [Test]
    public static void TestCharacterHashSet()
    {
        var charactersHashSet = new HashSet<Character>(Characters);
        Assert.That(charactersHashSet.Count, Is.EqualTo(Characters.Count));
    }

    [Test]

    public static void TestCharacterLookup()
    {
        var charactersLookup = Characters.ToLookup(character => character.RaceId);

        Assert.That(charactersLookup[GetRaceId(RaceNames.Orc)].Count(), Is.EqualTo(3));
        Assert.That(charactersLookup[GetRaceId(RaceNames.Elf)].Count(), Is.EqualTo(2));
        Assert.That(charactersLookup[GetRaceId(RaceNames.Human)].Count(), Is.EqualTo(3));
        Assert.That(charactersLookup[GetRaceId(RaceNames.Dwarf)].Count(), Is.EqualTo(1));
        Assert.That(charactersLookup[GetRaceId(RaceNames.Gnome)].Count(), Is.EqualTo(2));

    }

    [Test]
    public static void TestCharacterSort()
    {
        var charactersSorted = Characters.OrderBy(character => character.Health).ToList();
        Assert.That(charactersSorted[0].Name, Is.EqualTo(98));
        charactersSorted = Characters.OrderByDescending(character => character.isDead).ToList();
        Assert.That(charactersSorted[0].Name, Is.False);

    }

    [Test]
    public static void testCharacterGroupby()
    {
        var charactersGroupby = Characters.GroupBy(character => character.RaceId);
        Assert.That(charactersGroupby.Count(), Is.EqualTo(5));
    }

    [Test]
    public static void TestCharacterComplicatedOperations()
    { 
    var highestHealthByRacce =Races.Select(race => 
    Characters.Where(character => character.RaceId==race.ID)
    .MaxBy(character=> character.Health))
    .Where(character => character != null)
    .ToList();

    var averageHealthByRace=Races.Select(race=>
    Characters.Where(character => character.RaceId==race.ID)
    .Average(character => character.Health))
    .ToList();

     Assert.That(highestHealthByRacce.Count, Is.EqualTo(5));


    }

    [Test]
    public static void TestAnonymousType()
    {
        var charactersAnon = Characters.Select(character => new
        {
            character.Name,
            character.Health,
        });
        Assert.That(charactersAnon.First().Name, Is.EqualTo("Artur Pendragon"));
    }

    [Test]
    public static void TestSort()
    { 
    var orderedCharacters = Characters.OrderBy(el => el, new CharacterComparer()).ToList();
        Assert.That(orderedCharacters[0].Name, Is.EqualTo("Artur Pendragon"));
    }

    [Test]
    public static void TestToArray()
    {
        var arr=Characters.Select(el=>el.Health).ToArray();
        Assert.That(arr, Is.All.TypeOf<int>());
    }

    [Test]
    public static void TestSortt()
    {
        var orderedCharacters = Characters.OrderBy(el => el, new CharacterComparer()).ToList();
        Assert.That(orderedCharacters[0].Name, Is.EqualTo("Artur Pendragon"));
    }

    [Test]
    public static void TestFirstOrDefault()
    {
        var character = Characters.FirstOrDefault(el => el.Name == "Artur Pendragon");
        Assert.That(character?.Name, Is.EqualTo("Artur Pendragon"));
    }


}

public class CharacterComparer :IComparer<Character>
{
    public int Compare(Character? x, Character? y)
    {
        return String.Compare(x?.Name, y?.Name,StringComparison.Ordinal);
    }
}

public static class ListExtensions
{
    public static SortedList<TKey, TValue> ToSortedList<TKey, TValue>(this IEnumerable<TValue> source, Func<TValue, TKey> keySelector)
    {
        var sortedList = new SortedList<TKey, TValue>();
        foreach (var value in source)
        {
            sortedList.Add(keySelector(value), value);
        }
        return sortedList;
    }
}



class Program
{
    static void Main()
    {
        LINQTests.printCharacters();
    }
}