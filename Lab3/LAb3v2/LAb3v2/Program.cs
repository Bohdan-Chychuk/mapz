﻿using System.Text;
//using Microsoft.VisualStudio.TestTools.UnitTesting;
using NUnit.Framework;

using System.Collections.Generic;
using System.Linq;
using NUnit.Framework.Internal;
using NUnitLite;

public class Animal
{
    public string Name { get; private set; }
    public int Health { get; set; }

    public Guid SpeciesId { get; private set; }

    public bool IsSick => Health <= 0;

    public Animal(string name, Species species)
    {
        Name = name;
        SpeciesId = species.ID;
        Health = 100;
    }
}

public enum SpeciesNames
{
    Cow,
    Chicken,
    Pig,
    Sheep,
    Goat
}

public class Species
{
    public string Name { get; private set; }
    public Guid ID { get; private set; }

    public Species(string name)
    {
        Name = name;
        ID = Guid.NewGuid();
    }

    public Species(SpeciesNames speciesName)
    {
        Name = speciesName.ToString();
        ID = Guid.NewGuid();
    }
}

public static class FarmTests
{
    public static readonly List<Species> SpeciesList = new()
    {
        new Species(SpeciesNames.Cow),
        new Species(SpeciesNames.Chicken),
        new Species(SpeciesNames.Pig),
        new Species(SpeciesNames.Sheep),
        new Species(SpeciesNames.Goat)
    };

    public static Guid GetSpeciesId(SpeciesNames name) =>
        SpeciesList.Find(el => el.Name == name.ToString())?.ID ?? Guid.Empty;

    public static List<Animal> Animals = new()
    {
        new Animal("Bessie", SpeciesList[0]){ Health = 98 },
        new Animal("Clucky", SpeciesList[1]){ Health = 100 },
        new Animal("Porky", SpeciesList[2]){ Health = 100 },
        new Animal("Shaun", SpeciesList[3]){ Health = 100 },
        new Animal("Billy", SpeciesList[4]){ Health = 100 },
        new Animal("Bossy", SpeciesList[0]){ Health = 100 }
    };

    public static void PrintAnimals()
    {
        var sb = new StringBuilder();

        sb.AppendLine(String.Format("> {0,25} {1,40} {2,10} {3,7} {4,6}\n", "Name", "SpeciesId", "SpeciesName", "Health", "IsSick\n"));

        Animals.ForEach(animal
            => sb.AppendLine(String.Format("> {0,25} {1,40} {2,10} {3,7} {4,6}\n",
            animal.Name,
            animal.SpeciesId.ToString(),
            SpeciesList.Find(species => species.ID == animal.SpeciesId)?.Name ?? "Unknown",
            animal.Health.ToString(),
            animal.IsSick.ToString()
            )));

        Console.WriteLine(sb.ToString());
    }

    [Test]
    public static void TestAnimalsDictionary()
    {
        var animalsDict = Animals.ToDictionary(
            animal => animal.Name,
            animal => animal
        );
        Assert.That(animalsDict.Keys, Is.All.TypeOf<string>());
    }
    
    [Test]
    public static void TestAnimalSortedList()
    {
        var animalsSortedList = Animals.ToSortedList(animal => animal.Health);
        Assert.That(animalsSortedList.Values, Is.Ordered.By("Health"));
    }
    

    [Test]
    public static void TestAnimalQueue()
    {
        var animalsQueue = new Queue<Animal>(Animals);
        Assert.That(animalsQueue.Count, Is.EqualTo(Animals.Count));
    }

    [Test]
    public static void TestAnimalStack()
    {
        var animalsStack = new Stack<Animal>(Animals);
        Assert.That(animalsStack.Count, Is.EqualTo(Animals.Count));
    }

    [Test]
    public static void TestAnimalHashSet()
    {
        var animalsHashSet = new HashSet<Animal>(Animals);
        Assert.That(animalsHashSet.Count, Is.EqualTo(Animals.Count));
    }

    [Test]
    public static void TestAnimalLookup()
    {
        var animalsLookup = Animals.ToLookup(animal => animal.SpeciesId);

        Assert.That(animalsLookup[GetSpeciesId(SpeciesNames.Cow)].Count(), Is.EqualTo(2));
        Assert.That(animalsLookup[GetSpeciesId(SpeciesNames.Chicken)].Count(), Is.EqualTo(1));
        Assert.That(animalsLookup[GetSpeciesId(SpeciesNames.Pig)].Count(), Is.EqualTo(1));
        Assert.That(animalsLookup[GetSpeciesId(SpeciesNames.Sheep)].Count(), Is.EqualTo(1));
        Assert.That(animalsLookup[GetSpeciesId(SpeciesNames.Goat)].Count(), Is.EqualTo(1));
    }

    [Test]
    public static void TestAnimalSort()
    {
        var animalsSorted = Animals.OrderBy(animal => animal.Health).ToList();
        Assert.That(animalsSorted[0].Name, Is.EqualTo("Bessie"));
        animalsSorted = Animals.OrderByDescending(animal => animal.IsSick).ToList();
        Assert.That(animalsSorted[0].IsSick, Is.False);
    }

    [Test]
    public static void TestAnimalGroupBy()
    {
        var animalsGroupBy = Animals.GroupBy(animal => animal.SpeciesId);
        Assert.That(animalsGroupBy.Count(), Is.EqualTo(5));
    }

    [Test]
    public static void TestAnimalComplicatedOperations()
    {
        var highestHealthBySpecies = SpeciesList.Select(species =>
            Animals.Where(animal => animal.SpeciesId == species.ID)
            .MaxBy(animal => animal.Health))
            .Where(animal => animal != null)
            .ToList();

        var averageHealthBySpecies = SpeciesList.Select(species =>
            Animals.Where(animal => animal.SpeciesId == species.ID)
            .Average(animal => animal.Health))
            .ToList();

        Assert.That(highestHealthBySpecies.Count, Is.EqualTo(5));
    }
    

    [Test]
    public static void TestAnonymousType()
    {
        var animalsAnon = Animals.Select(animal => new
        {
            animal.Name,
            animal.Health,
        });
        Assert.That(animalsAnon.First().Name, Is.EqualTo("Bessie"));
    }

    [Test]
    public static void TestSort()
    {
        var orderedAnimals = Animals.OrderBy(el => el, new AnimalComparer()).ToList();
        Assert.That(orderedAnimals[0].Name, Is.EqualTo("Bessie"));
    }

    [Test]
    public static void TestToArray()
    {
        var arr = Animals.Select(el => el.Health).ToArray();
        Assert.That(arr, Is.All.TypeOf<int>());
    }

    [Test]
    public static void TestSortAgain()
    {
        var orderedAnimals = Animals.OrderBy(el => el, new AnimalComparer()).ToList();
        Assert.That(orderedAnimals[0].Name, Is.EqualTo("Bessie"));
    }

    [Test]
    public static void TestFirstOrDefault()
    {
        var animal = Animals.FirstOrDefault(el => el.Name == "Bessie");
        Assert.That(animal?.Name, Is.EqualTo("Bessie"));
    }


    [Test]
    public static void TestDictionaryOperations()
    {
        var animalsDict = Animals.ToDictionary(
                       animal => animal.Name,
                                  animal => animal
                                         );

        Assert.That(animalsDict["Bessie"].Name, Is.EqualTo("Bessie"));

    }

}

public class AnimalComparer : IComparer<Animal>
{
    public int Compare(Animal? x, Animal? y)
    {
        return String.Compare(x?.Name, y?.Name, StringComparison.Ordinal);
    }
}

public static class ListExtensions
{
    public static SortedList<TKey, TValue> ToSortedList<TKey, TValue>(this IEnumerable<TValue> source, Func<TValue, TKey> keySelector)
    {
        var sortedList = new SortedList<TKey, TValue>();
        foreach (var value in source)
        {
            sortedList.Add(keySelector(value), value);
        }
        return sortedList;
    }
}

//class Program
//{
//    static int Main(string[] args)
//    {
//        return new AutoRun().Execute(args);
       
//    }
//}
class Program
{
    static void Main()
    {
        FarmTests.PrintAnimals();

    }
}
