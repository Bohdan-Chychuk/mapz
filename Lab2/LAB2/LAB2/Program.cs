﻿public interface IAnimal
{
    void Eat();
    void Sleep();
}

public abstract class Animal : IAnimal
{
    //Name of the animal
    public string Name { get; set; }
    private int Age { get; set; }
    protected int Weight { get; set; }
    public abstract void Eat();
    public abstract void Sleep();
}

public class Dog : Animal
{
    //protected при наслідуванні: доступ до поля з класу нащадка:
    public void SetWeight(int weight)
    {
        Weight = weight;
    }
    //метод гет і сет для поля Age
    public int Age
    {
        get { return Age; }
        set { Age = value; }
    }


    public override void Eat()
    {
        Console.WriteLine("Dog is eating");
    }

    public override void Sleep()
    {
        Console.WriteLine("Dog is sleeping");
    }
}



//інший клас Animal2 без модифікаторів доступу
class Animal2 
{
    string Name { get; set; }
    int Age { get; set; }
    int Weight { get; set; }
   void Eat()
    {
        Console.WriteLine("Animal is eating");
    }
    void Sleep()
    {
        Console.WriteLine("Animal is sleeping");
    }

    class Class1
    {
        void Method1()
        {
            Animal2 animal = new Animal2();
            animal.Eat();
            animal.Sleep();
            string name = animal.Name;
        }
    }

    struct Struct1
    {
        void Method1()
        {
            Animal2 animal = new Animal2();
            animal.Eat();
            animal.Sleep();
            string name = animal.Name;
        }
    }
    interface Interface1
    {
        void Method1()
        {
            Animal2 animal = new Animal2();
            animal.Eat();
            animal.Sleep();
            string name = animal.Name;
        }
    }

    private class Class2
    { }

    //public Class2 class2 = new;
}

public enum DogColor
{
    NotVisible=0,
    Orange = 10,
    White = 1,
    Black = 2,
    BlackandWhite = White | Black,
    AlmostGray = White<<Black,
    Alien= Orange>>White,
    Tasty=Orange^ Black,
    NotBlack = ~Black,

}
//Приклад множинного наслідування
public interface FakeInterface
{
    void FakeMethod();
}
public abstract class FakeClass
{
    public abstract void FakeMethod();
}
public class FakeClass2 : FakeClass, FakeInterface
{
    public override void FakeMethod()
    {
        Console.WriteLine("FakeMethod");
    }
}

//Клас для прикладу перевантаження конструкторів,функцій
public class Example
{

    public Example()
    {
        Console.WriteLine("Constructor1");
    }
    public Example(int a,int b)
    {
        Console.WriteLine("Constructor2");
    }
    public void Method()
    {
        Console.WriteLine("Method1");
    }
    public void Method(int a)
    {
        Console.WriteLine("Method2");
    }
    //Різні варіанти використання ключового слова this
    public Example(int a)
    {
        this.Method();
        this.Method(1);
    }
    public Example(int a, int b, int c) : this(a, b)
    {
        Console.WriteLine("Constructor3");
    }
    // Різні варіанти використання ключового слова base
    public Example(int a, int b, int c, int d) : base()
    {
        Console.WriteLine("Constructor4");
    }


}


/*Реалізувати різні види ініціалізації полів як статичних так
 * і динамічних: за допомогою статичного та динамічного конструктора
 * , та ін. Дослідити у якій послідовності ініціалізуються поля.*/

public class Example2
{
    //Ініціалізація в момент оголошення 
    public int a = 1;
    //Ініціалізація в конструкторі
    public Example2()
    {
        a = 2;
        Console.WriteLine("Constructor");
    }
    //Статична ініціалізація в момент завантаження класу
    public static int b = 3;
    //Статична ініціалізація в статичному конструкторі
    static Example2()
    {
        b = 4;
        Console.WriteLine("Static constructor");
    }
    



}


/*
  Реалізувати функції з параметрами out, ref. Показати відмінності при
наявності та без цих параметрів. Показати випадки, коли ці параметри не
мають значення. 
 */

public static class TestRefAndOut
{
    //Методи з параметрами out, ref які змінюють значення параметрів
    public static void ChangeValue(ref int a)
    {
        a = 10;
    }

    public static void ChangeValue1(out int a)
    {
        a = 10;
    }

    //Методи без параметрів out, ref
    public static void WithoutOutAndRef(int a)
    {
        a = 10;
    }


    public static void Test()
    {
        int a = 1;


        //Методи з параметрами out, ref які змінюють значення параметрів
        ChangeValue(ref a);
        Console.WriteLine("Ref що змінює:");
        Console.WriteLine(a);

        ChangeValue1(out a);
        Console.WriteLine("Out що змінює:");
        Console.WriteLine(a);

        a=0;
    
        //Методи без параметрів out, ref
        WithoutOutAndRef(a);
        Console.WriteLine("Без out, ref:");
        Console.WriteLine(a);





    }








}


// Клас Dog
public class Dog2
{
    public string Name { get; set; }
    public int Age { get; set; }

    public Dog2(string name, int age)
    {
        Name = name;
        Age = age;
    }

    public void Bark()
    {
        Console.WriteLine("Woof!");
    }
}

// Структура DogStruct
public struct DogStruct
{
    public string Name { get; set; }
    public int Age { get; set; }

    public DogStruct(string name, int age)
    {
        Name = name;
        Age = age;
    }

    public void Bark()
    {
        Console.WriteLine("Woof!");
    }
}


//Продемонструвати boxing / unboxing
public static class TestBoxing
{
    public static void Test()
    {
        int a = 1;
        object obj = a; //boxing
        Console.WriteLine($"Boxed value: { obj}");
        obj = 2;
        int b = (int)obj; //unboxing
        Console.WriteLine($"Unboxed value: {b}");
    }
}

/*
  Реалізувати явні та неявні оператори приведення то іншого типу (implicit 
та explicit)
 */

public class Example4
{
    public int Value { get; set; }
    public Example4(int value)
    {
        Value = value;
    }
    public static implicit operator int(Example4 example)
    {
        return example.Value;
    }
    public static explicit operator Example4(int value)
    {
        return new Example4(value);
    }
}

//Перевизначити і дослідити методи класу object (у тому числі і protected методи)
public class Example5
{
    public int Value { get; set; }
    public Example5(int value)
    {
        Value = value;
    }
    public override string ToString()
    {
        return Value.ToString();
    }
    public override bool Equals(object obj)
    {
        if (obj is Example5)
        {
            return Value == ((Example5)obj).Value;
        }
        return false;
    }
    public override int GetHashCode()
    {
        return Value.GetHashCode();
    }
    public void Method()
    {
        Console.WriteLine("Method");
    }
}




class Program
{
    static void Main(string[] args)
    {
        //Dog dog = new Dog();
        //dog.Eat();
        //dog.Sleep();
        //string name = dog.Name;

        //Console.WriteLine(DogColor.AlmostGray & DogColor.Black);
        // Console.WriteLine(DogColor.White | DogColor.Black);

        //Демонстрація послідовності виклику конструкторів
        //Example2 example = new Example2();
        // TestRefAndOut.Test();

        //TestBoxing.Test();

        //Example4 example = new Example4(10);
        //int value = example;
        //Example4 example2 = (Example4)10;

        Example5 example = new Example5(10);
        Console.WriteLine(example.ToString());
        Console.WriteLine(example.Equals(new Example5(10)));
        Console.WriteLine(example.GetHashCode());
        example.Method();



    }
}


