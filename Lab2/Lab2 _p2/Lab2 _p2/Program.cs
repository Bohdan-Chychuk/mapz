﻿using System;
using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Running;

namespace ArithmeticAndStringOperationsBenchmark
{
    public class ArithmeticOperations
    {
        public double Add(double a, double b) => a + b;
        public double Subtract(double a, double b) => a - b;
        public double Multiply(double a, double b) => a * b;
        public double Divide(double a, double b) => a / b;
    }

    public class StringOperations
    {
        public string Concatenate(string a, string b) => a + b;
        public string ToUpperCase(string input) => input.ToUpper();
        public string Reverse(string input)
        {
            char[] charArray = input.ToCharArray();
            Array.Reverse(charArray);
            return new string(charArray);
        }
    }

    public class Benchmarks
    {
        private readonly ArithmeticOperations _arith = new ArithmeticOperations();
        private readonly StringOperations _strOps = new StringOperations();

        [Benchmark]
        public void BenchmarkAdd() => _arith.Add(5, 3);

        [Benchmark]
        public void BenchmarkSubtract() => _arith.Subtract(5, 3);

        [Benchmark]
        public void BenchmarkMultiply() => _arith.Multiply(5, 3);

        [Benchmark]
        public void BenchmarkDivide() => _arith.Divide(5, 3);

        [Benchmark]
        public void BenchmarkConcatenate() => _strOps.Concatenate("Hello", "World");

        [Benchmark]
        public void BenchmarkToUpperCase() => _strOps.ToUpperCase("hello");

        [Benchmark]
        public void BenchmarkReverse() => _strOps.Reverse("hello");
    }

    class Program
    {
        static void Main(string[] args)
        {
            var summary = BenchmarkRunner.Run<Benchmarks>();
        }
    }
}
