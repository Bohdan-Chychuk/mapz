```

BenchmarkDotNet v0.13.12, Windows 11 (10.0.22621.3593/22H2/2022Update/SunValley2)
Intel Core i5-10300H CPU 2.50GHz, 1 CPU, 8 logical and 4 physical cores
.NET SDK 8.0.205
  [Host]     : .NET 8.0.5 (8.0.524.21615), X64 RyuJIT AVX2 [AttachedDebugger]
  DefaultJob : .NET 8.0.5 (8.0.524.21615), X64 RyuJIT AVX2


```
| Method               | Mean       | Error     | StdDev    | Median     |
|--------------------- |-----------:|----------:|----------:|-----------:|
| BenchmarkAdd         |  0.0155 ns | 0.0147 ns | 0.0130 ns |  0.0165 ns |
| BenchmarkSubtract    |  0.0031 ns | 0.0082 ns | 0.0072 ns |  0.0000 ns |
| BenchmarkMultiply    |  0.0086 ns | 0.0132 ns | 0.0103 ns |  0.0061 ns |
| BenchmarkDivide      |  0.0101 ns | 0.0166 ns | 0.0147 ns |  0.0000 ns |
| BenchmarkConcatenate |  6.1367 ns | 0.2242 ns | 0.6324 ns |  6.0331 ns |
| BenchmarkToUpperCase | 20.1582 ns | 0.4277 ns | 1.1342 ns | 19.9402 ns |
| BenchmarkReverse     | 15.9037 ns | 0.3487 ns | 0.9428 ns | 15.7199 ns |
