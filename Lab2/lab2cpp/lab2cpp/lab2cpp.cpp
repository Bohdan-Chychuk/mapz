#include <iostream>
#include <chrono>
#include <string>
#include <algorithm>

class ArithmeticOperations {
public:
    double Add(double a, double b) { return a + b; }
    double Subtract(double a, double b) { return a - b; }
    double Multiply(double a, double b) { return a * b; }
    double Divide(double a, double b) { return a / b; }
};

class StringOperations {
public:
    std::string Concatenate(const std::string& a, const std::string& b) { return a + b; }
    std::string ToUpperCase(const std::string& input) {
        std::string result = input;
        std::transform(result.begin(), result.end(), result.begin(), ::toupper);
        return result;
    }
    std::string Reverse(const std::string& input) {
        std::string result = input;
        std::reverse(result.begin(), result.end());
        return result;
    }
};

int main() {
    const int iterations = 100000;

    ArithmeticOperations arith;
    StringOperations strOps;

    // Timing arithmetic operations
    auto start = std::chrono::high_resolution_clock::now();
    for (int i = 0; i < iterations; i++) {
        arith.Add(5, 3);
        arith.Subtract(5, 3);
        arith.Multiply(5, 3);
        arith.Divide(5, 3);
    }
    auto end = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double> elapsed = end - start;
    std::cout << "Arithmetic operations: " << elapsed.count() * 1000 << " ms\n";

    // Timing string operations
    start = std::chrono::high_resolution_clock::now();
    for (int i = 0; i < iterations; i++) {
        strOps.Concatenate("Hello", "World");
        strOps.ToUpperCase("hello");
        strOps.Reverse("hello");
    }
    end = std::chrono::high_resolution_clock::now();
    elapsed = end - start;
    std::cout << "String operations: " << elapsed.count() * 1000 << " ms\n";

    return 0;
}
