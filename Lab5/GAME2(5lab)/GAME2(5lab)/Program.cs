﻿using System;
using System.Resources;




// Використання патерну Singleton для створення єдиного ресурсного менеджера
public class ResourceManager
{
    private static ResourceManager _instance;
    public int Money { get; private set; }
    public int Food { get; private set; }
    public int Water { get; private set; }

    private ResourceManager()
    {
        Money = 100;
        Food = 50;
        Water = 50;
    }

    public static ResourceManager Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = new ResourceManager();
            }
            return _instance;
        }
    }

    public void AddMoney(int amount)
    {
        Money += amount;
    }

    public void UseFood(int amount)
    {
        Food -= amount;
    }

    public void UseWater(int amount)
    {
        Water -= amount;
    }
}


// Використання патерну Абстрактна фабрика для створення рослин та тварин
public abstract class Plant
{
    public abstract void Grow();
}

public abstract class Animal
{
    public abstract void Feed();
}

public class Corn : Plant
{
    public override void Grow()
    {
        Console.WriteLine("Corn is growing");
    }
}

public class Cow : Animal
{
    public override void Feed()
    {
        Console.WriteLine("Cow is being fed");
    }
}

public abstract class FarmFactory
{
    public abstract Plant CreatePlant();
    public abstract Animal CreateAnimal();
}

public class ConcreteFarmFactory : FarmFactory
{
    public override Plant CreatePlant()
    {
        return new Corn();
    }

    public override Animal CreateAnimal()
    {
        return new Cow();
    }
}



// Використання патерну Прототип для клонування об'єктів

public abstract class PlantPrototype
{
    public abstract PlantPrototype Clone();
}

public abstract class AnimalPrototype
{
    public abstract AnimalPrototype Clone();
}

public class CornPrototype : PlantPrototype
{
    public int GrowthStage { get; private set; }

    public CornPrototype(int growthStage)
    {
        GrowthStage = growthStage;
    }

    public override PlantPrototype Clone()
    {
        return new CornPrototype(this.GrowthStage);
    }

    public void Grow()
    {
        GrowthStage++;
        Console.WriteLine($"Corn is growing to stage {GrowthStage}");
    }
}

public class CowPrototype : AnimalPrototype
{
    public int Health { get; private set; }

    public CowPrototype(int health)
    {
        Health = health;
    }

    public override AnimalPrototype Clone()
    {
        return new CowPrototype(this.Health);
    }

    public void Feed()
    {
        Health += 10;
        Console.WriteLine($"Cow's health is now {Health}");
    }
}
//Використання патерну Фасад для спрощення взаємодії з підсистемою
public class FarmFacade
{
    private ResourceManager resourceManager;
    private FarmFactory farmFactory;

    public FarmFacade()
    {
        resourceManager = ResourceManager.Instance;
        farmFactory = new ConcreteFarmFactory();
    }

    public void GrowPlant()
    {
        var plant = farmFactory.CreatePlant();
        plant.Grow();
    }

    public void FeedAnimal()
    {
        var animal = farmFactory.CreateAnimal();
        animal.Feed();
    }

    public void AddResources(int money, int food, int water)
    {
        resourceManager.AddMoney(money);
        // food and water addition can be added in the ResourceManager if needed
    }

    public void DisplayResources()
    {
        Console.WriteLine($"Money: {resourceManager.Money}");
        Console.WriteLine($"Food: {resourceManager.Food}");
        Console.WriteLine($"Water: {resourceManager.Water}");
    }
}

// Використання патерну Декоратор для додавання функціональності до об'єктів

public abstract class AnimalDecorator : Animal
{
    protected Animal _animal;

    public AnimalDecorator(Animal animal)
    {
        _animal = animal;
    }

    public override void Feed()
    {
        _animal.Feed();
    }
}

public class HealthBoostedAnimal : AnimalDecorator
{
    public HealthBoostedAnimal(Animal animal) : base(animal) { }

    public override void Feed()
    {
        base.Feed();
        Console.WriteLine("Animal's health is boosted!");
    }
}



// Використання патерну Проксі для контролю доступу до ресурсів
public interface IResourceManager
{
    int Money { get; }
    int Food { get; }
    int Water { get; }
    void AddMoney(int amount);
    void UseFood(int amount);
    void UseWater(int amount);
}

public class ResourceManagerProxy : IResourceManager
{
    private ResourceManager _realResourceManager;

    public ResourceManagerProxy()
    {
        _realResourceManager = ResourceManager.Instance;
    }

    public int Money => _realResourceManager.Money;
    public int Food => _realResourceManager.Food;
    public int Water => _realResourceManager.Water;

    public void AddMoney(int amount)
    {
        if (amount > 0)
        {
            _realResourceManager.AddMoney(amount);
        }
    }

    public void UseFood(int amount)
    {
        if (amount <= _realResourceManager.Food)
        {
            _realResourceManager.UseFood(amount);
        }
    }

    public void UseWater(int amount)
    {
        if (amount <= _realResourceManager.Water)
        {
            _realResourceManager.UseWater(amount);
        }
    }
}
























class Program
{
    static void Main(string[] args)
    {
        // Використання Фасаду
        var farmFacade = new FarmFacade();
        farmFacade.DisplayResources();
        farmFacade.GrowPlant();
        farmFacade.FeedAnimal();

        // Використання Декоратора
        var cow = new Cow();
        var boostedCow = new HealthBoostedAnimal(cow);
        boostedCow.Feed();

        // Використання Проксі
        IResourceManager resourceManagerProxy = new ResourceManagerProxy();
        resourceManagerProxy.AddMoney(50);
        resourceManagerProxy.UseFood(10);
        resourceManagerProxy.UseWater(5);
        Console.WriteLine($"Money: {resourceManagerProxy.Money}");
        Console.WriteLine($"Food: {resourceManagerProxy.Food}");
        Console.WriteLine($"Water: {resourceManagerProxy.Water}");
    }
}
