﻿using System;
using System.Resources;




// Використання патерну Singleton для створення єдиного ресурсного менеджера
//public class ResourceManager
//{
//    private static ResourceManager _instance;
//    public int Money { get; private set; }
//    public int Food { get; private set; }
//    public int Water { get; private set; }

//    private ResourceManager()
//    {
//        Money = 100;
//        Food = 50;
//        Water = 50;
//    }

//    public static ResourceManager Instance
//    {
//        get
//        {
//            if (_instance == null)
//            {
//                _instance = new ResourceManager();
//            }
//            return _instance;
//        }
//    }

//    public void AddMoney(int amount)
//    {
//        Money += amount;
//    }

//    public void UseFood(int amount)
//    {
//        Food -= amount;
//    }

//    public void UseWater(int amount)
//    {
//        Water -= amount;
//    }
//}


// Використання патерну Абстрактна фабрика для створення рослин та тварин
public abstract class Plant
{
    public abstract void Grow();
}

public abstract class Animal
{
    public abstract void Feed();
}

public class Corn : Plant
{
    private IPlantState currentState;

    public Corn()
    {
        currentState = new SeedState();
    }

    public void SetState(IPlantState state)
    {
        currentState = state;
    }

    public override void Grow()
    {
        currentState.Grow(this);
    }
}


public class Cow : Animal
{
    public override void Feed()
    {
        Console.WriteLine("Cow is being fed");
    }
}

public abstract class FarmFactory
{
    public abstract Plant CreatePlant();
    public abstract Animal CreateAnimal();
}

public class ConcreteFarmFactory : FarmFactory
{
    public override Plant CreatePlant()
    {
        return new Corn();
    }

    public override Animal CreateAnimal()
    {
        return new Cow();
    }
}



// Використання патерну Прототип для клонування об'єктів

public abstract class PlantPrototype
{
    public abstract PlantPrototype Clone();
}

public abstract class AnimalPrototype
{
    public abstract AnimalPrototype Clone();
}

public class CornPrototype : PlantPrototype
{
    public int GrowthStage { get; private set; }

    public CornPrototype(int growthStage)
    {
        GrowthStage = growthStage;
    }

    public override PlantPrototype Clone()
    {
        return new CornPrototype(this.GrowthStage);
    }

    public void Grow()
    {
        GrowthStage++;
        Console.WriteLine($"Corn is growing to stage {GrowthStage}");
    }
}

public class CowPrototype : AnimalPrototype
{
    public int Health { get; private set; }

    public CowPrototype(int health)
    {
        Health = health;
    }

    public override AnimalPrototype Clone()
    {
        return new CowPrototype(this.Health);
    }

    public void Feed()
    {
        Health += 10;
        Console.WriteLine($"Cow's health is now {Health}");
    }
}
//Використання патерну Фасад для спрощення взаємодії з підсистемою
public class FarmFacade
{
    private ResourceManager resourceManager;
    private FarmFactory farmFactory;

    public FarmFacade()
    {
        resourceManager = ResourceManager.Instance;
        farmFactory = new ConcreteFarmFactory();
    }

    public void GrowPlant()
    {
        var plant = farmFactory.CreatePlant();
        plant.Grow();
    }

    public void FeedAnimal()
    {
        var animal = farmFactory.CreateAnimal();
        animal.Feed();
    }

    public void AddResources(int money, int food, int water)
    {
        resourceManager.AddMoney(money);
        // food and water addition can be added in the ResourceManager if needed
    }

    public void DisplayResources()
    {
        Console.WriteLine($"Money: {resourceManager.Money}");
        Console.WriteLine($"Food: {resourceManager.Food}");
        Console.WriteLine($"Water: {resourceManager.Water}");
    }
}

// Використання патерну Декоратор для додавання функціональності до об'єктів

public abstract class AnimalDecorator : Animal
{
    protected Animal _animal;

    public AnimalDecorator(Animal animal)
    {
        _animal = animal;
    }

    public override void Feed()
    {
        _animal.Feed();
    }
}

public class HealthBoostedAnimal : AnimalDecorator
{
    public HealthBoostedAnimal(Animal animal) : base(animal) { }

    public override void Feed()
    {
        base.Feed();
        Console.WriteLine("Animal's health is boosted!");
    }
}



// Використання патерну Проксі для контролю доступу до ресурсів
public interface IResourceManager
{
    int Money { get; }
    int Food { get; }
    int Water { get; }
    void AddMoney(int amount);
    void UseFood(int amount);
    void UseWater(int amount);
}

public class ResourceManagerProxy : IResourceManager
{
    private ResourceManager _realResourceManager;

    public ResourceManagerProxy()
    {
        _realResourceManager = ResourceManager.Instance;
    }

    public int Money => _realResourceManager.Money;
    public int Food => _realResourceManager.Food;
    public int Water => _realResourceManager.Water;

    public void AddMoney(int amount)
    {
        if (amount > 0)
        {
            _realResourceManager.AddMoney(amount);
        }
    }

    public void UseFood(int amount)
    {
        if (amount <= _realResourceManager.Food)
        {
            _realResourceManager.UseFood(amount);
        }
    }

    public void UseWater(int amount)
    {
        if (amount <= _realResourceManager.Water)
        {
            _realResourceManager.UseWater(amount);
        }
    }
}




//Поведінковий патерн Ланцюжок відповідальності
public abstract class ResourceHandler
{
    protected ResourceHandler successor;

    public void SetSuccessor(ResourceHandler successor)
    {
        this.successor = successor;
    }

    public abstract void HandleRequest(string resourceType, int amount);
}

public class MoneyHandler : ResourceHandler
{
    public override void HandleRequest(string resourceType, int amount)
    {
        if (resourceType == "Money")
        {
            ResourceManager.Instance.AddMoney(amount);
        }
        else if (successor != null)
        {
            successor.HandleRequest(resourceType, amount);
        }
    }
}

public class FoodHandler : ResourceHandler
{
    public override void HandleRequest(string resourceType, int amount)
    {
        if (resourceType == "Food")
        {
            ResourceManager.Instance.UseFood(amount);
        }
        else if (successor != null)
        {
            successor.HandleRequest(resourceType, amount);
        }
    }
}

public class WaterHandler : ResourceHandler
{
    public override void HandleRequest(string resourceType, int amount)
    {
        if (resourceType == "Water")
        {
            ResourceManager.Instance.UseWater(amount);
        }
        else if (successor != null)
        {
            successor.HandleRequest(resourceType, amount);
        }
    }
}



//Поведінковий патерн Стан
public interface IPlantState
{
    void Grow(Corn corn);
}

public class SeedState : IPlantState
{
    public void Grow(Corn corn)
    {
        Console.WriteLine("Кукурудза проростає із насіння.");
        corn.SetState(new SproutState());
    }
}

public class SproutState : IPlantState
{
    public void Grow(Corn corn)
    {
        Console.WriteLine("Кукурудза росте від паростка до зрілої рослини.");
        corn.SetState(new MatureState());
    }
}

public class MatureState : IPlantState
{
    public void Grow(Corn corn)
    {
        Console.WriteLine("Кукурудза повністю виросла і готова до збору.");
    }
}






//Поведынковий патерн Спостерігач
public interface IResourceObserver
{
    void Update();
}


public interface IResourceSubject
{
    void Attach(IResourceObserver observer);
    void Detach(IResourceObserver observer);
    void Notify();
}


public class ResourceManager : IResourceSubject
{
    private static ResourceManager _instance;
    public int Money { get; private set; }
    public int Food { get; private set; }
    public int Water { get; private set; }

    private List<IResourceObserver> observers = new List<IResourceObserver>();

    private ResourceManager()
    {
        Money = 100;
        Food = 50;
        Water = 50;
    }

    public static ResourceManager Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = new ResourceManager();
            }
            return _instance;
        }
    }

    public void Attach(IResourceObserver observer)
    {
        observers.Add(observer);
    }

    public void Detach(IResourceObserver observer)
    {
        observers.Remove(observer);
    }

    public void Notify()
    {
        foreach (var observer in observers)
        {
            observer.Update();
        }
    }

    public void AddMoney(int amount)
    {
        Money += amount;
        Notify();
    }

    public void UseFood(int amount)
    {
        Food -= amount;
        Notify();
    }

    public void UseWater(int amount)
    {
        Water -= amount;
        Notify();
    }
}

public class ResourceDisplay : IResourceObserver
{
    public void Update()
    {
        Console.WriteLine($"Ресурси оновлено: Гроші = {ResourceManager.Instance.Money}, Їжа = {ResourceManager.Instance.Food}, Вода = {ResourceManager.Instance.Water}");
    }
}





// Головний клас програми
public class Program
{
    public static void Main(string[] args)
    {
        // Ініціалізація спостерігача
        ResourceDisplay display = new ResourceDisplay();
        ResourceManager.Instance.Attach(display);

        // Ініціалізація обробників ресурсів
        MoneyHandler moneyHandler = new MoneyHandler();
        FoodHandler foodHandler = new FoodHandler();
        WaterHandler waterHandler = new WaterHandler();

        moneyHandler.SetSuccessor(foodHandler);
        foodHandler.SetSuccessor(waterHandler);

        // Додавання ресурсів
        Console.WriteLine("Додавання ресурсів:");
        moneyHandler.HandleRequest("Money", 50);
        foodHandler.HandleRequest("Food", 10);
        waterHandler.HandleRequest("Water", 5);

        // Використання ресурсів
        Console.WriteLine("\nВикористання ресурсів:");
        moneyHandler.HandleRequest("Money", -20);
        foodHandler.HandleRequest("Food", -5);
        waterHandler.HandleRequest("Water", -2);

        // Зростання кукурудзи
        Console.WriteLine("\nЗростання кукурудзи:");
        Corn corn = new Corn();
        corn.Grow(); // SeedState -> SproutState
        corn.Grow(); // SproutState -> MatureState
        corn.Grow(); // MatureState (already fully grown)

        // Завершення гри
        Console.WriteLine("\nГра завершена.");
    }
}














