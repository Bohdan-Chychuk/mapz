﻿using System;




// Використання патерну Singleton для створення єдиного ресурсного менеджера
public class ResourceManager
{
    private static ResourceManager _instance;
    public int Money { get; private set; }
    public int Food { get; private set; }
    public int Water { get; private set; }

    private ResourceManager()
    {
        Money = 100;
        Food = 50;
        Water = 50;
    }

    public static ResourceManager Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = new ResourceManager();
            }
            return _instance;
        }
    }

    public void AddMoney(int amount)
    {
        Money += amount;
    }

    public void UseFood(int amount)
    {
        Food -= amount;
    }

    public void UseWater(int amount)
    {
        Water -= amount;
    }
}


// Використання патерну Абстрактна фабрика для створення рослин та тварин
public abstract class Plant
{
    public abstract void Grow();
}

public abstract class Animal
{
    public abstract void Feed();
}

public class Corn : Plant
{
    public override void Grow()
    {
        Console.WriteLine("Corn is growing");
    }
}

public class Cow : Animal
{
    public override void Feed()
    {
        Console.WriteLine("Cow is being fed");
    }
}

public abstract class FarmFactory
{
    public abstract Plant CreatePlant();
    public abstract Animal CreateAnimal();
}

public class ConcreteFarmFactory : FarmFactory
{
    public override Plant CreatePlant()
    {
        return new Corn();
    }

    public override Animal CreateAnimal()
    {
        return new Cow();
    }
}



// Використання патерну Прототип для клонування об'єктів

public abstract class PlantPrototype
{
    public abstract PlantPrototype Clone();
}

public abstract class AnimalPrototype
{
    public abstract AnimalPrototype Clone();
}

public class CornPrototype : PlantPrototype
{
    public int GrowthStage { get; private set; }

    public CornPrototype(int growthStage)
    {
        GrowthStage = growthStage;
    }

    public override PlantPrototype Clone()
    {
        return new CornPrototype(this.GrowthStage);
    }

    public void Grow()
    {
        GrowthStage++;
        Console.WriteLine($"Corn is growing to stage {GrowthStage}");
    }
}

public class CowPrototype : AnimalPrototype
{
    public int Health { get; private set; }

    public CowPrototype(int health)
    {
        Health = health;
    }

    public override AnimalPrototype Clone()
    {
        return new CowPrototype(this.Health);
    }

    public void Feed()
    {
        Health += 10;
        Console.WriteLine($"Cow's health is now {Health}");
    }
}
























class Program
{
    static void Main(string[] args)
    {
        // Приклад використання Singleton
        var resources = ResourceManager.Instance;
        Console.WriteLine(resources.Money);
        Console.WriteLine(resources.Food);
        Console.WriteLine(resources.Water);


        // Приклад використання Абстрактної фабрики
        var factory = new ConcreteFarmFactory();
        var plant = factory.CreatePlant();
        var animal = factory.CreateAnimal();

        plant.Grow();
        animal.Feed();

        // Приклад використання Прототипа
        var originalCorn = new CornPrototype(1);
        var clonedCorn = originalCorn.Clone() as CornPrototype;

        var originalCow = new CowPrototype(100);
        var clonedCow = originalCow.Clone() as CowPrototype;

        clonedCorn.Grow();
        clonedCow.Feed();

    }
}
